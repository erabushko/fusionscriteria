# FusionsCriteria

## Manuscript title 
Experimentally Deduced Criteria For Detection of Clinically Relevant Fusion 3’ Oncogenes From FFPE bulk RNA sequencing data. 

## Authors
- Elizaveta Rabushko
- Maksim Sorokin
- Maria Suntsova
- Aleksander Seryakov
- Denis Kuzmin
- Elena Poddubskaya
- Anton Buzdin

## Description
The project enables to get count of alignments from multiple position-sorted and indexed BAM files that overlap genes exons in a BED file; and double normalize obtained read counts per exon length and sequencing depth.

## Usage
The BEDtools multicov software is used for determining exon coverage of specific genes, then obtained read counts are normalised in Python.  

## Support
If you have any question regarding the project, feel free to ask elnrabush@gmail.com







