import pandas as pd
import matplotlib.pyplot as plt
import numpy as np

df = pd.read_csv("OUTPUT_file.txt", sep='\t', header=None, low_memory=False) # input file with read counts per exon

# calculate exons length 
exon_len = []
for i in range(a): # a - number of gene exons
    lngh = df[2][i]-df[1][i]
    exon_len.append(lngh)
# normalize read counts per length
normd = df[4]/exon_len

 
x = np.arange(1, a+1)
plt.bar(x, height = normd) # create diagramm
plt.show()

# additional total read counts normalization for FS1, LuC46 and OC11 between samples comparison 
total_counts = pd.read_csv("total_read_counts.rtf", sep=' ', header=None) # input file with total read counts
total_counts = total_counts.set_index(0)
second_normd = normd*1000000/total_counts.loc['{sample name}'][1] # insert specific sample name
